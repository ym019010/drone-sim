package drone_sim;

import java.util.Scanner;

import javax.swing.JFileChooser;

import java.io.*;

import java.util.concurrent.TimeUnit;

import javax.swing.filechooser.FileFilter;

public class DroneInterface {
	private Scanner s;						// scanner used for input from user
    private DroneArena myArena;				// arena in which drones are shown
    /**
    	 * constructor for DroneInterface
    	 * sets up scanner used for input and the arena
    	 * then has main loop allowing user to enter commands
     */
   public DroneInterface() {
	   	
    	 s = new Scanner(System.in);			// set up scanner for user input
    	 int arenaSizeX = 0;
    	 int arenaSizeY = 0;
    	 
    	 myArena = new DroneArena(0, 0);	// creates a drone arena of (0,0) expecting the user to input their desired dimensions
    	
        char ch = ' ';
        do {
        	System.out.print("\nEnter (N)ew Arena, (A)dd drone, get (I)nformation, Show (D)isplay, (M)ove Drone once, Move Drone (T)en Times, (F)ile options or e(X)it > ");
        	ch = s.next().charAt(0);
        	s.nextLine();
        	switch (ch) {
    			case 'A' :
    			case 'a' :
        					myArena.addDrone();	// add a new drone to arena
        					break;
        		case 'I' :
        		case 'i' :
        					System.out.print(myArena.toString());
            				break;
        		case 'x' : 	ch = 'X';				// when X detected program ends
        					break;
        		case 'D' :
        		case 'd' :
        					doDisplay();
        					break;
        		case 'M':
    			case 'm':
    						myArena.moveAllDrones(myArena); // Moves all drones
    						doDisplay(); // Displays Arena with drones in new position
    				break;
    			case 'N':
    			case 'n':
    				/*
    				 * User can create a new arena with the following code 
    				 */
       				System.out.print("\n Enter New Arena Dimensions");
    				System.out.print("\nX Value> ");
    				try {
    					arenaSizeX = s.nextInt(); // checks input
    				} catch (Exception a) { // catches exception
    					System.err.println("Only numbers allowed"); // prints warning message
    					System.out.print("X Value> "); // asks user to reenter a value
    					s.nextLine();
    					arenaSizeX = s.nextInt();
    				}
    				// repeats above but for Y dimension
    				System.out.print("\nY value > ");
    				try {
    					arenaSizeY = s.nextInt();
    				} catch (Exception b) {
    					System.err.println("Only numbers allowed");
    					System.out.print("Y value > ");
    					s.nextLine();

    					arenaSizeY = s.nextInt();
    				}
    				myArena = new DroneArena(arenaSizeX, arenaSizeY); // arena is created with user input
    				break;
    			case 'T':
    			case 't':
    				/*
    				 * The following code moves all drones ten times,
    				 * checking to see if the arena is empty and 
    				 * gives an error message as appropriate
    				 * including error handling for the interruption
    				 * of movement.
    				 */
    				if (myArena.droneList.isEmpty() == false) {
    					for (int i = 0; i < 10; i++) {
    						System.out.println("-----------------------------------");
    						myArena.moveAllDrones(myArena);
    						doDisplay();
    						System.out.print(myArena.toString());
    						try {
    							TimeUnit.MILLISECONDS.sleep(200); // 200ms delay
    						}catch (InterruptedException c) {
    							System.err.format("IOException: %s%n", c);
    						}
    					}
    				} else if (myArena.droneList.isEmpty() == true && myArena.getX() > 0 && myArena.getY() > 0) {
    					System.err.println("No drones to move!"); // checks if list is empty and produces error if it is
    				}
    				break;
    			case 'F':
    			case 'f':
    				fileOptions();


        	}
    		} while (ch != 'X');						
        
       s.close();									// close scanner
    }
    
	public static void main(String[] args) {
		DroneInterface r = new DroneInterface();	// just call the interface
	}
	
	void doDisplay() {
			if(myArena.getX() > 0 && myArena.getY() > 0){
			
			ConsoleCanvas c = new ConsoleCanvas(myArena.getX() + 2, myArena.getY() + 2);
			//call showDrones suitably
			myArena.showDrones(c);
			//then use the ConsoleCanvas.toString method 
			System.out.println(c.toString());
			if(myArena.droneList.isEmpty()){
				System.err.println("0 Drones Added yet!");
			}
		}
	}
	
	
	void fileOptions(){
		s = new Scanner(System.in); //scanner
		char ch  = ' ';
		System.out.print("\nEnter (S)ave File,(L)oad File or e(X)it >");
		ch = s.next().charAt(0);
		s.nextLine();
		switch (ch) {
			case'S':
			case 's':
				try{ saveFile();
				} 	catch (Exception e){
				System.out.print("");
				}
				break;
		
			case 'L':
			case 'l':
				try{
					loadFile();
				}
				catch(Exception a){
					System.out.print("");
					
				}
				break;
			case 'x':
				ch = 'X';
				break;
			default:
				break;
				
		}
		
	}
	
	void saveFile() throws IOException {

		// JFileChooser properties
		JFileChooser chooser = new JFileChooser("E:\\Part 2\\Java\\Drone_Sim\\Files");// The desired directory
		chooser.setDialogTitle("Save arena to: "); // Window title
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES); // Chooses what files are shown
		// this where the windows opens
		int returnVal = chooser.showOpenDialog(null); // stores if user clicks open/cancel
		if (returnVal == JFileChooser.APPROVE_OPTION) { // if user presses open
			File userFile = chooser.getSelectedFile(); // gets the selected file
			System.out.println("Arena saved!\n" + "File Name: " + userFile.getName() + "\nDirectory: "
					+ userFile.getAbsolutePath()); // prints out the file name and directory
			// Saving Process
			FileWriter fileWriter = new FileWriter(userFile); // creates a new file writer
			BufferedWriter writer = new BufferedWriter(fileWriter); // adds to buffer
			// Saves arena dimensions on first line
			writer.write(Integer.toString(myArena.arenaSizeX));
			writer.write(" ");
			writer.write(Integer.toString(myArena.arenaSizeY));
			writer.newLine(); // new line
			// every line stores the X,Y information of each drone
			for (Drone d : myArena.droneList) {
				writer.write(Integer.toString(d.getX()));
				writer.write(" ");
				writer.write(Integer.toString(d.getY()));
				writer.write(" ");
				writer.write(Integer.toString(d.getDirection().ordinal()));
				writer.newLine();
			}
			writer.close();
		}
	}


	void loadFile() throws IOException {

		
		JFileChooser chooser = new JFileChooser("E:\\Part 2\\Java\\Drone_Sim\\Files"); // load from this directory
		chooser.setDialogTitle("Load arena from: "); //window title
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		String fileContents = " ";
		//Opens Window
		int returnVal = chooser.showOpenDialog(null); //stores if user selects open/cancel 
		if (returnVal == JFileChooser.APPROVE_OPTION) { //if user presses open
			File userFile = chooser.getSelectedFile(); //gets the selected file
			if (chooser.getSelectedFile().isFile()) { // if the file exists
				System.out.println("Arena Loaded!\n" + "File Name: " + userFile.getName() + "\nDirectory: "
						+ userFile.getAbsolutePath()); 

				//clears the drone list
				if (!myArena.droneList.isEmpty()) {
					myArena.droneList.clear();
				}
				//loads
				FileReader fileReader = new FileReader(userFile);
				BufferedReader reader = new BufferedReader(fileReader);

				fileContents = reader.readLine();
				String[] loadSize = fileContents.split(" "); 
				int loadX = Integer.parseInt(loadSize[0]); //x coordinate
				int loadY = Integer.parseInt(loadSize[1]); //y coordinate
				myArena = new DroneArena(loadX, loadY); 
				while (fileContents != null) { 
					fileContents = reader.readLine();
					String[] numbers = fileContents.split(" ");
					int x = Integer.parseInt(numbers[0]); // x coordinate
					int y = Integer.parseInt(numbers[1]); // y coordinate
					int ordinal = Integer.parseInt(numbers[2]); //direction
				
					myArena.droneList.add(new Drone(x, y, Direction.values()[ordinal]));

				}
				reader.close();
			}
		}

	}

		
	
	
}