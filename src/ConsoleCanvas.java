package drone_sim;

public class ConsoleCanvas {
	char[][] canvas;
	private int drawX;
	private int drawY;
	
	// constructor including 
	ConsoleCanvas(int arrX, int arrY){
		drawX = arrX;
		drawY = arrY;
		canvas = new char[arrX ][arrY ];
		for(int i = 0; i < drawX; i++) {
			for(int j = 0; j < drawY; j++) { 
				canvas[i][j] = ' ';
				if (i == 0) {
					canvas[i][j] = '#';
				}
				if (j == 0) {
					canvas[i][j] = '#';
				}
				if (i == drawX - 1) {
					canvas[i][j] = '#';
				}
				if (j == drawY - 1) {
					canvas[i][j] = '#';
				}
			}
		}
	}
	
	public void showIt(int xPos, int yPos, char image){
		canvas[xPos + 1][yPos + 1] = image;
	}
	
	public String toString() {
		String create = "";
		for (int i = 0; i < drawX ; i++) {
			for (int j = 0; j < drawY; j++) { 
				create = create + canvas[i][j] + " " ;
			}
			create = create + "\n";
		}
		return create;
	}

	
	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas (5, 50);	// create a canvas
		c.showIt(9,9,'D');								// add a Drone at 4,3
		System.out.println(c.toString());				// display result
	}
	 
}
