package drone_sim;

import java.util.Random;


public enum Direction {
	 NORTH, EAST, SOUTH, WEST;
	
	public static Direction getRandom(){
		
		Random rand = new Random();
		return values()[rand.nextInt(values().length)];
	}

	public Direction nextDirection() {
		 return values()[(this.ordinal() + 1) % values().length];
	}

}
