package drone_sim;
import java.util.Random;
import java.util.ArrayList;


public class DroneArena{
	public int arenaSizeX;
	public int arenaSizeY;
	ArrayList<Drone> droneList;
	private Drone[] drone_list;
	private Random randomGenerator;
	private static int NumDrones;
	Drone randDrone;
	

	
	public DroneArena(int x, int y){
	 arenaSizeX= x;
	 arenaSizeY= y ;
	 droneList = new ArrayList<Drone>();
	 randomGenerator = new Random(); //initialises of random number
	 drone_list = new Drone[x*y]; //initialises drone list
	}
	
	// retrieves drone based on given coordinates
	public Drone getDroneAt(int x, int y) {
		Drone drone = null;
		for(Drone a : droneList) {
			if(a.isHere(x, y) == true) {
				drone = a;
			}
		}
		return drone;
	}
	
	
	/*
	 * adds drones to arena list
	 * 
	 * checks if arena is full 
	 * 
	 * randomly generates a number until an empty
	 * position is found and creates a new drone
	 * 
	 * produces a message if arena is full
	 */
	public void addDrone(){
		int randX;
		int randY; 
		
		if(droneList.size() < arenaSizeX * arenaSizeY) {
			do {
				 randX = randomGenerator.nextInt(arenaSizeX);
				 randY = randomGenerator.nextInt(arenaSizeY);
			}while(getDroneAt(randX, randY) != null);
			randDrone = new Drone(randX, randY, Direction.getRandom());
			droneList.add(randDrone);
		}
		else {
			System.err.println("The arena is Full");
			
		}		
		drone_list[NumDrones]= randDrone;
		NumDrones ++;

		
	}
	
	
	// getter functions to return private attributes of arena
	public int getX() {
		return arenaSizeX;
	}
	
	public int getY(){
		return arenaSizeY;
	}
	
	//shows drones on a canvas
	public void showDrones(ConsoleCanvas c) {
		for(Drone d : droneList) {
			d.displayDrone(c);
		}
	}
	/*
	 * 	toString displays the arena dimensions and drone information checking if the arena 
	 * exists
	 */
	public String toString() {	
		String output = "";
		if (arenaSizeX > 0 && arenaSizeY > 0 && droneList.isEmpty() == false){ //checks both exist
			output = "";
			output += "Current arena size is ("+ arenaSizeX + "," + arenaSizeY + ")" + " The Number of Drones Are: " + NumDrones ;
		for (int i = 0; i < droneList.size(); i++) {
			output += "\n" + droneList.get(i).toString();
		}
		}
		else if ( arenaSizeX > 0 && arenaSizeY > 0 && droneList.isEmpty() == true){ // checks if arena exists but no drones
			output += "Current droneList Size is" + arenaSizeX + "," + arenaSizeY; 
					System.err.print("\n There are no drones to display information about!");
		}
		else{ //checks if nothing exists
			output += "\n The arena is empty! \n";
		}
			output = output + "\n";
		return output;
		
		
			}
	public boolean canMoveHere(int x, int y) {
		if (getDroneAt(x, y) != null || x >= arenaSizeX || y >= arenaSizeY || x < 0 || y < 0) {
			return false;
		} else {
			return true;
		}
	}

	public void moveAllDrones(DroneArena a){
		for (Drone d : droneList)
			d.tryToMove(a);
	}
		
	}
	