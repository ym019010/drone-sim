package drone_sim;

public class Drone {
	public int id ; // id of the drone
	private static int idPlusOne = 1; //constantly ensures id adds for each drone
	private int xPos; //drone x coordinate
	private int yPos; //drone y coordinate
	private Direction direct; //direction
	
	
	//constructor
	public Drone(int x, int y, Direction d) {
		xPos = x;
		yPos = y;
		id = idPlusOne;
		idPlusOne = idPlusOne + 1;
		direct = d;
		
	}
	//getters 
	public int getX() {
		return xPos;
	}
	
	public int getY() {
		return yPos;
	}
	
	public Direction getDirection(){
		return direct;
	}
	
	
	public boolean isHere(int x, int y) {
		boolean status = false;
		if (x == xPos && y == yPos) {
			status = true;
		}
		return status;
	}

	//outputs info when moving ten times or info is required
	public String toString() {
		String res="Drone "+ id +" is at"+ "("+xPos+","+ yPos+")" + " facing " + direct;
		return res;
		}
	
	/*
	 * display the drone in the canvas
	 */

	public void displayDrone(ConsoleCanvas c) {
		char dronePic = 'D';
		c.showIt(xPos, yPos, dronePic);
	
	}
	/*
	 * checks if the space the drone is meant to move into is empty
	 * and if it can, it will move the drone to that position
	 */
	public void tryToMove(DroneArena a){
		switch(direct){
		case SOUTH:
			if(a.canMoveHere(xPos + 1, yPos)) 
			{
				direct = direct.getRandom();
				xPos = xPos + 1;
				}
			else
				direct = direct.nextDirection();
			break;
		case WEST:
			if(a.canMoveHere(xPos, yPos - 1)) 
			{
				direct = direct.getRandom();
				yPos = yPos - 1;
			}
				else 
				direct = direct.nextDirection();
			break;
		case NORTH:
			if(a.canMoveHere(xPos - 1, yPos)) {
				direct = direct.getRandom();
				xPos = xPos - 1;
			}
				
			else 
				direct = direct.nextDirection();
				break;
		case EAST:
			if(a.canMoveHere(xPos, yPos + 1)) 
			{
				direct = direct.getRandom();
				yPos = yPos + 1;
			}
			else
				direct = direct.nextDirection();
			break;
		
			
		}
	}
}	
